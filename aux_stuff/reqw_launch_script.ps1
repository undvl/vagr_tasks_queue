Param(
  [string]$port,
  [string]$maxpause,
  [string]$taskid,
  [string]$p1,
  [string]$p2,
  [string]$p3
)

Start-Process c:\tmp\reqw.exe -ArgumentList $port, $maxpause, $taskid, $p1, $p2, $p3 -RedirectStandardOutput '.\console.out' -RedirectStandardError '.\console.err'
