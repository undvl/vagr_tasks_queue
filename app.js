const nodeEnv = process.env.NODE_ENV||'development',
      config = require('./config.json'),
      cfg = config[nodeEnv];

const async = require('async');

const express = require('express'),
      app = express(),
      server = require('http').Server(app),
      port = cfg.launcher.port;
const bodyParser = require('body-parser');
const routes = require('./routes');

const assert = require('assert');

const MongoClient = require('mongodb').MongoClient;
const dbUrl = `mongodb://${cfg.mongo.host}:${cfg.mongo.port}/${cfg.mongo.db}`;

const vagrant = require('./srv/vagrant.js');


MongoClient.connect(dbUrl, (err, db) => {
  assert.equal(null, err);

  let gTasksQueue = [];
  // DEL: exemple date
  // gTasksQueue = [{ taskid: 4, params: { p1: 't3_v1', p2: 't3_v2', p3: 't3_v3' } }];

  const redis = require("redis"),
        redCli = redis.createClient({
          host: cfg.redis.host,
          port: cfg.redis.port,
          db: cfg.redis.db
        });

  // DEL:
  // some sample data

  // redCli.hmset('vm:0', ['taskid', 22], redis.print);
  // redCli.sadd('vms', 'vm:0', redis.print);
  // redCli.hget('vm:0', 'taskid', redis.print);
  // redCli.hmset('vm:1', ['taskid', 33], redis.print);
  // redCli.sadd('vms', 'vm:1', redis.print);
  // redCli.smembers('vms', redis.print);

  // srem vms vm:4 - vm del example




  const DBTasks = require('./srv/dbTasks');
  const dbTasks = new DBTasks({ cfg, db, redCli, tasksQueue: gTasksQueue });

  /**
   * Main queue processor
   */
  let asyncQTasks = async.queue((processingTask, callback) => {
    console.log('processingTask.taskid: '+processingTask.taskid);
    gTasksQueue.shift();

    async.waterfall([
      // ищем своюодную виртуалку
      dbTasks.checkFreeVM,
      // пишем в редис активный таск
      (freeVM, cb) => {
        dbTasks.addActiveVmToRedis(freeVM, processingTask, (err, freeVM) => {
          if (err) return cb(err);
          return cb(null, freeVM);
        });
      },
      // запускаем виртуалку
      (freeVM, cb) => {
        let keys = Object.keys(processingTask.params);
        let paramStr = keys.map(key => `-${key} '${processingTask.params[key]}'`)
          .join(' ');
        paramStr = `-taskid ${processingTask.taskid} ` + paramStr;

        vagrant.startVmAndTask(cfg, freeVM, paramStr, (err, freeVM) => {
          if (err) return cb(err);
          console.log('VM SUCCESSFULY STARTED: '+freeVM);
          return cb(null, freeVM);
        });
      }

    ], (err, result) => { // waterfall callback
      if (err) {
         console.log(err);
         // Откладываем следующию итерацию, в случии ошибки
         setTimeout(() => asyncQTasks.resume(), cfg.durQueueProcessorPauseOnError);
         asyncQTasks.pause();

         // Если виртуалки заняты, возвращаем задачу в начало очереди
         if(err === 'FreeVM not found') {
           asyncQTasks.unshift(processingTask);
           gTasksQueue.unshift(processingTask);
         }
         return callback();
      }

      return callback();
    });

  }); // используем 1 воркер (поведение async.queue по умолчанию) // "}, 1);"

  asyncQTasks.drain = function() {
    console.info('Tasks Queue is empty!');
  };


  /**
   * Создаем луп добавляющий новые задачи из монги в очередь
   */ 
  let fn = function() {
    dbTasks.getTasksFromDB((err, newTasks) => {
      gTasksQueue.push(...newTasks);
      asyncQTasks.push(newTasks);
    });
  };
  setInterval(fn, cfg.intervalLoadNewTasks);
  fn();
  /**/


  /**
   * EXPRESS
   */
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true}));

  routes({ app, db, redCli, vagrant, tasksQueue: gTasksQueue });

  server.listen(port, function() {
    console.log(`listening on ${port}`);
  })
});
