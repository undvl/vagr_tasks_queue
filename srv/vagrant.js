const async = require('async');
const exec = require('child_process').exec;

function startVmAndTask(cfg, vagrVmNum, paramStr, callback) {
  async.series([
    cb => {
      exec("vagrant up", {
        cwd: cfg.vagrantBoxes,
        env: Object.assign({}, process.env, { VAGR_VM_NUM: vagrVmNum })
      }, function (err, stdout, stderr) {
        if (err) return cb(err);

        console.log(stdout);
        cb(null, `success: vm ${vagrVmNum} up`);
      });
    },
    cb => {
      let str = `-port "${cfg.launcher.port}" -maxpause "${cfg.launcher.maxPause}"`
      str += ' '+paramStr;
      console.log('PARAM STRING: '+str);
      exec("vagrant provision --provision-with app-start", {
        cwd: cfg.vagrantBoxes,
        env: Object.assign({}, process.env, {
          VAGR_VM_NUM: vagrVmNum,
          SHELL_ARGS: str
        })
      }, function (err, stdout, stderr) {
        if (err) return cb(err);

        console.log(stdout);
        // cb(null, `success: vm ${vagrVmNum} app-start`);
      });

      // Вынесено из exec колбека, чтобы не ждать задержки в программе
      cb(null, `success: vm ${vagrVmNum} app-start`);
    }
  ], (err, results) => {
    if(err) { callback(err); return console.log(err); }

    console.log(results);
    callback(null, results);
  });
}

function shutdownVM(cfg, vagrVmNum, callback) {
  exec("vagrant halt", {
    cwd: cfg.vagrantBoxes,
    env: Object.assign({}, process.env, { VAGR_VM_NUM: vagrVmNum })
  }, function (err, stdout, stderr) {
    if (err) { callback(err); return console.log(err); }

    console.log(stdout);
    const results = `vm ${vagrVmNum} shutdown success`;
    console.log(results);
    callback(null, results);
  });
}

module.exports = { startVmAndTask, shutdownVM };
