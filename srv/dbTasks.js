const async = require('async');

function DBTasks({ cfg, db, redCli, tasksQueue }) {
  /**
   * Получение новых задач
   * callback (err, tasksQueue)
   **/
  this.getTasksFromDB = function(callback) {

    // Запрашиваем задачи в монге
    function reqMongo(cb) {
      const gt = cfg.mongo.getTasks;

      db.collection(gt.collection)
        .find(gt.filter, { _id: 0 })
        .sort(gt.sort)
        .limit(gt.limit)
        .toArray((err, mongoTasks) =>
      {
        // if (err) return console.log(err);
        // console.log(mongoTasks);
        cb(null, mongoTasks) // cb
      });
    }

    // сверяем их с активными в редисе и очередью 
    function compareWithRedisAndQueue(mongoTasks, cb) {
      // получаем список активных виртуалок
      redCli.smembers('vms', (err, vms) => {
        // достаем taskid для этих виртуалок 
        async.map(
          vms,
          (vm, callback) => redCli.hget(vm, 'taskid', (err, taskid) => {
            if (err) return callback(err);

            callback(null, Number(taskid));
          }),
          (err, activeTasks) => {
            if (err) return cb(err);
            console.log('activeTasks: '+activeTasks);

            // создаем объединенный массив айдишников
            // активных задач и задач в очереди
            let currentTasks = tasksQueue.map(v => v.taskid)
              .concat(activeTasks.map(v => Number(v)));
            // console.log(currentTasks);

            // и отфильтровывем их из задач полученых с монги
            let newTasks = mongoTasks.filter(task => {
              if (currentTasks.indexOf(task.taskid) === -1) return true;
              return false;
            });

            console.log('newTasks:');
            console.log(newTasks);
            return cb(null, newTasks); // cb
          }
        );
      });
    }

    async.waterfall([reqMongo, compareWithRedisAndQueue], (err, newTasks) => {
      // передаем новую очередь для присвоения
      callback(null, newTasks);
    });
  };

  /**
   * Проверка, есть ли свободная виртуалка
   * Возвращает ее номер
   */
  this.checkFreeVM = function(callback) {
    redCli.smembers('vms', (err, results) => {
      if (err) return callback(err);
      
      var vmsInUse = results.map(v => Number(v.replace('vm:', '')));
      var freeVM;

      for (var i = 0; i < cfg.numVMs; i++) {
        if (vmsInUse.indexOf(i) === -1) {
          freeVM = i; break;
        }
      }
      console.log('freeVM: '+freeVM);

      if (typeof freeVM !== 'undefined') return callback (null, freeVM);
      return callback ('FreeVM not found');
    });
  };

  /**
   * Резервирует активную виртуалку в редисе
   * и пишет туда taskid и дату запуска
   */
  this.addActiveVmToRedis = function(freeVM, processingTask, callback) {
    redCli.multi()
      .hmset(`vm:${freeVM}`, [
        'taskid', processingTask.taskid,
        'begin_time', new Date().getTime()
      ])
      .sadd('vms', `vm:${freeVM}`)
      .exec((err, results) => {
        if(err) return callback(err);

        // Проверяем, что sadd действительно добавила запись
        if (results[1] === 1) return callback(null, freeVM);
        return callback('addActiveVmToRedis Failed');
      });
  };
}

module.exports = DBTasks;