const nodeEnv = process.env.NODE_ENV||'development',
      config = require('./../config.json'),
      cfg = config[nodeEnv];

const async = require('async');

// Обработчик сообщения от активных тасков
function AppReqHandler({ db, redCli, vagrant }) {

  // ищет номер виртуалки по id задачи
  function findVmNum(taskid, callback) {
    redCli.smembers('vms', (err, vms) => {
      vms.forEach(vm => {
        redCli.hget(vm, 'taskid', (err, r) => {
          if (r == taskid) {
            callback( null, Number(vm.replace('vm:','')) );
            clearTimeout(errCallbackTimer);
          }
        });
      });
    });
    // Добавляем коллбэк ошибки для вотерфолла, если не найдено
    let errCallbackTimer = setTimeout(() => callback('VM not found'), 2000);
  }

  // зактывает виртуалку и удаляет из редиса
  function delVM(vmNum, callback) {
    callback(null, vmNum);
    // Делаем таймаут, чтобы provision успел отработать 
    // и не заблокировал vagrant halt
    setTimeout(() => {
      vagrant.shutdownVM(cfg, vmNum, () => {
        redCli.srem('vms', `vm:${vmNum}`, () => {
          // callback(null, vmNum);
        });
      });
    }, 10000);
  }

  // findVmNum + delVM
  function releaseVM(taskid, callback) {
    async.waterfall([
      cb => {
        findVmNum(taskid, (err, vmNum) => {
          if (err) return cb(err);
          cb(null, vmNum);
        });
      }, 
      (vmNum, cb) => {
        delVM(vmNum, (err, vmNum) => {
          if (err) return cb(err);
          cb(null, vmNum);
        });
      }
    ], (err, vmNum) => {
      if (err) return callback(err);
      callback(null, vmNum);
    });
  }

  this.saveReport = function(req, res, next) {
    const taskid = req.body.taskid;
    const data = req.body.data;

    async.series([
      // insert report to mongo
      cb => {
        db.collection(cfg.mongo.saveReport.collection)
          .insertOne({ taskid: Number(taskid), data }, (err, r) => {
            if(err) return cb(err);
            cb(null, `saveReport inserted to mongo, taskid: ${taskid}`);
          });
      },
      cb => {
        // помечаем задачу выполненной
        db.collection(cfg.mongo.getTasks.collection)
          .update(
            { taskid: Number(taskid) },
            { $set: { isProcessed: 1 } },
            (err, r) =>
          {
            if(err) return cb(err);
            cb(null, `saveReport marked as processed, taskid: ${taskid}`);
          });
      },
      // release VM
      cb => {
        releaseVM(taskid, (err, vmNum) => {
          if(err) return cb(err);
          cb(null, `vm:${vmNum} closed succesfully`);
        });
      }
    ], (err, results) => {
      if(err) return next(err);

      console.log(results);
      res.send(results);
    });

  };

  this.saveError = function(req, res, next) {
    const taskid = req.body.taskid;
    const data = req.body.data;

    async.series([
      // insert error to mongo
      cb => {
        db.collection(cfg.mongo.saveError.collection)
          .insertOne({ taskid: Number(taskid), data }, (err, r) => {
            if(err) return cb(err);
            cb(null, `saveError inserted to mongo, taskid: ${taskid}`);
          });
      },
      cb => {
        // увеличиваем счетчик ошибок задачи
        db.collection(cfg.mongo.getTasks.collection)
          .update(
            { taskid: Number(taskid) },
            { $inc: { errCount: 1 } },
            (err, r) =>
          {
            if(err) return cb(err);
            cb(null, `saveError incresed error counter, taskid: ${taskid}`);
          });
      },
      // release VM
      cb => {
        releaseVM(taskid, (err, vmNum) => {
          if(err) return cb(err);
          cb(null, `vm:${vmNum} closed succesfully`);
        });
      }
    ], (err, results) => {
      if(err) return next(err);

      console.log(results);
      res.send(results);
    });

  };
}

module.exports = AppReqHandler;
