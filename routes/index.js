const AppReqHandler = require('./appReqHandler');
const RptApiHandler = require('./rptApiHandler')

module.exports = function({ app, db, redCli, vagrant, tasksQueue }) {
  const appReqHandler = new AppReqHandler({ db, redCli, vagrant });
  const rptApiHandler = new RptApiHandler({ db, redCli, tasksQueue });

  app.post('/saveReport', appReqHandler.saveReport);
  app.post('/saveError', appReqHandler.saveError);

  app.get('/api/get_tasks_queue', rptApiHandler.getTasksQueue);
  app.get('/api/get_tasks_active', rptApiHandler.getTasksActive);
  app.get('/api/get_tasks_reports', rptApiHandler.getTasksReports);
  app.get('/api/get_tasks_errors', rptApiHandler.getTasksErrors);
  app.get('/api/get_tasks_exceeded', rptApiHandler.getTasksExceeded);
}