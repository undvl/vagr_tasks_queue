const nodeEnv = process.env.NODE_ENV||'development',
      config = require('./../config.json'),
      cfg = config[nodeEnv];

const async = require('async');

// Обработчик JSON API
function RptApiHandler({ db, redCli, tasksQueue }) {

  // очередь задач
  this.getTasksQueue = function(req, res, next) {
    res.send(tasksQueue);
  };

  // активные задачи
  this.getTasksActive = function(req, res, next) {
    redCli.smembers('vms', (err, vms) => {
      async.map(
        vms,
        (vm, callback) => redCli.hgetall(vm, (err, task) => {
          if (err) return callback(err);

          // достаем с монги остальную инфу по задаче
          db.collection(cfg.mongo.getTasks.collection)
            .findOne({ taskid: Number(task.taskid) }, { _id: 0}, (err, r) => {
              if (err) return callback(err);

              task.vm = vm.replace('vm:', '');
              console.log(task.taskid);
              console.log(r);
              task.params = r.params;
              task.errCount = r.errCount;
              callback(null, task);
            });
        }),
        (err, activeTasks) => {
          if (err) return next(err);
          res.send(activeTasks);
        }
      );
    });
  };

  // отчеты задач
  this.getTasksReports = function(req, res, next) {
    db.collection(cfg.mongo.saveReport.collection)
        .find({}, { _id: 0 })
        .sort({ taskid: -1 })
        .limit(cfg.mongo.apiRowsLimit)
        .toArray((err, tasksReports) =>
      {
          if (err) return next(err);
          res.send(tasksReports);
      });
  };

  // ошибки задач
  this.getTasksErrors = function(req, res, next) {
    db.collection(cfg.mongo.saveError.collection)
        .find({}, { _id: 0 })
        .sort({ taskid: -1 })
        .limit(cfg.mongo.apiRowsLimit)
        .toArray((err, tasksErrors) =>
      {
          if (err) return next(err);
          res.send(tasksErrors);
      });
  };

  // отчет по убитым по таймаутам задачам
  this.getTasksExceeded = function(req, res, next) {
    db.collection(cfg.wd.collection)
        .find({}, { _id: 0 })
        .sort({ closed_time: -1 })
        .limit(cfg.mongo.apiRowsLimit)
        .toArray((err, tasksErrors) =>
      {
          if (err) return next(err);
          res.send(tasksErrors);
      });
  };

}

module.exports = RptApiHandler;